// console.log('Hello Edmar');

// Template Literals
let number = 6;
let getCube = (number ** 3);
console.log(`The cube of ${number} is ${getCube}`);

const address = ['Lot 10', 'BLK 169', 'Western Bicutan', 'Taguig City', 'Philippines'];
const [lotNumber, blockNumber, barangay, city, country] =  address;
console.log(`I live at ${lotNumber} ${blockNumber}, ${barangay} ${city}, ${country}`);



// Object Destructuring
const animal = {
	name: 'Vishwamali',
	nickname: 'Mali',
	gender: 'Female',
	type: 'Asian Elephant',
	zoo: 'Manila Zoo',
	age: 48
};

const {name, nickname, gender, type, zoo, age} = animal;

console.log(`${name}, commonly known as ${nickname}, is a ${gender} ${type} which is best known for being a major attraction of ${zoo} in Manila, Philippines. She is now ${age} years old.`);



// Arrow Functions
const numberArray = [1, 2, 3, 4, 5];
numberArray.forEach((number) => console.log(number));

let reduceNumber = numberArray.reduce((accumulator, currentValue) => {
	return accumulator + currentValue;
});
console.log(reduceNumber);



// Javascript Objects
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const hachiko = new Dog('Hachiko', 10, 'White Akita');
console.log(hachiko);